import { Dropbox } from "dropbox";
import { parseQueryString } from "../libs/utils";

const CLIENT_ID = "aqv7ycp649xack6";
const AUTH_URL = "http://localhost:3000/";

export function getAccessTokenFromUrl() {
  return parseQueryString(window.location.hash).access_token;
}

export function isAuthenticated() {
  return !!getAccessTokenFromUrl();
}

export async function getFiles(path = "") {
  const dbx = new Dropbox({
    accessToken: getAccessTokenFromUrl(),
  });
  return await dbx.filesListFolder({ path });
}

export async function getAuthenticationUrl() {
  const dbx = new Dropbox({ clientId: CLIENT_ID });
  return await dbx.auth.getAuthenticationUrl(AUTH_URL);
}

async function getLinks(dbx, path) {
  const {
    result: { links },
  } = await dbx.sharingListSharedLinks({ path });

  if (!links.length) {
    const {
      result: { url },
    } = await dbx.sharingCreateSharedLinkWithSettings({ path });
    return url;
  }

  const [{ url }] = links;
  return url;
}

export async function getDownloadLink(path) {
  const dbx = new Dropbox({ accessToken: getAccessTokenFromUrl() });
  const { result } = await dbx.sharingGetSharedLinkFile({
    url: await getLinks(dbx, path),
  });
  return window.URL.createObjectURL(new Blob([result.fileBlob]));
}
