import { getAuthenticationUrl } from "../../libs/dropbox";
import Dialog from "../Dialog";
import styles from "./styles.css";


let shadow;
export default class Login extends HTMLElement {
  constructor() {
    super();
    shadow = this.attachShadow({ mode: "open" });
    const style = document.createElement("style");
    style.textContent = styles;
    shadow.appendChild(style);

    const div = document.createElement("div");
    getAuthenticationUrl().then((authenticationUrl) => {
      div.innerHTML = `
        <div class="wrapper">
          <div class="title">
            <h1>Hello, please log in to start</h1>
            <a href="${authenticationUrl}" id="authlink" class="button">Authenticate</a>
          </div>
        <div>
    `;
    });

    this.shadowRoot.append(div);
  }
}

customElements.define("general-login", Login);
