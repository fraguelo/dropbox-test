import styles from "./styles.css";

let shadow;
export default class Spinner extends HTMLElement {
  constructor() {
    super();
    shadow = this.attachShadow({ mode: "open" });
    const styleSpinner = document.createElement("style");
    styleSpinner.textContent = styles;
    shadow.appendChild(styleSpinner);
  }

  async connectedCallback() {
    const div = document.createElement("div");
    div.innerHTML = `<span id='loader' class='loader' ></span>`;
    this.shadowRoot.append(div);
  }
}

customElements.define("general-spinner", Spinner);
