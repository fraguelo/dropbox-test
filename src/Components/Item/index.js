import { getDownloadLink } from "../../libs/dropbox";
import styles from "./styles.css";
import Spinner from "../Spinner";
import List from "../List";
import Dialog from "../Dialog";

let shadow;
export default class Item extends HTMLElement {
  constructor() {
    super();
    shadow = this.attachShadow({ mode: "open" });
    const style = document.createElement("style");
    style.textContent = styles;
    shadow.appendChild(style);
  }

  renderSubFolderItem() {
    const { name, path } = this.attributes;
    return `<div class="item subfolder" onclick='this.getRootNode().host.openFolder("${name.value}","${path.value}" )'  > 
      <div class="filename" >
        ${name.value}
      </div>
    </div>`;
  }

  renderFileItem() {
    const { name, path, type, date } = this.attributes;
    const formatedDate = new Date(date?.value);
    return `<div class="item file">
    <div class="filename">
      ${name.value}
    </div>
      <div id="options">
        <div class="date">${formatedDate.toLocaleString()}</div>
        <div class="linkWrapper">
          <a id="thelink" href="javascript:" onclick="this.getRootNode().host.getDownloadLink('${
            path.value
          }','${name.value}')" >
            <span class="download">Download</span>
          </a>
          <general-spinner id='loading'></general-spinner>
        </div>
      </div>
    </div>`;
  }

  async connectedCallback() {
    try {
      const { name, path, type, date } = this.attributes;
      const div = document.createElement("div");

      div.innerHTML =
        type.value === "file"
          ? this.renderFileItem()
          : this.renderSubFolderItem();
      shadow.append(div);
    } catch (e) {
      Dialog.show({
        title: "Error!",
        description: "Something went wrong",
      });
    }
  }

  openFolder(name, path) {
    this.getRootNode().host.goFromItem(name,path);
  }

  async getDownloadLink(path, name) {
    if (this.getRootNode().host.isDownloading()) {
      Dialog.show({
        title: "Wait!",
        description: "There is another file being downloaded",
      });
      return false;
    }
    this.getRootNode().host.toggleDownload();

    const a = document.createElement("a");
    a.classList.add("hidden");

    this.shadowRoot.getElementById("loading").style.display = "inline-block";
    this.shadowRoot.getElementById("thelink").style.display = "none";
    const blobLink = await getDownloadLink(path);

    this.shadowRoot.getElementById("loading").style.display = "none";
    this.shadowRoot.getElementById("thelink").style.display = "inline-block";

    a.setAttribute("href", blobLink);
    a.setAttribute("target", "_blank");
    a.setAttribute("download", name);
    this.shadowRoot.append(a);
    a.click();
    this.getRootNode().host.toggleDownload();
  }
}

customElements.define("general-item", Item);
