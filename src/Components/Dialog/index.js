import styles from "./styles.css";

let shadow;
export default class Dialog extends HTMLElement {
  constructor() {
    super();
    this.internals_ = this.attachInternals();
    shadow = this.attachShadow({ mode: "open" });
    const style = document.createElement("style");
    style.textContent = styles;
    shadow.appendChild(style);
  }

  async connectedCallback() {
    const div = document.createElement("div");
    div.innerHTML = `
    <div id="dialog" class="wrapperDialog hide">
        <div class='dialog' >
        <div class="header redWarning"></div>
        <div id="title" class="title redWarning">Test</div>
        <div id="description" class="description">Something went wrong!</div>
        <div class="buttonContainer "><button id='okButton' class='redWarning' type='button'/>OK</div>
        </div>
    </div>
    `;

    this.shadowRoot.append(div);
  }

  static getDialog() {
    const dialogComponent = document.querySelectorAll("general-dialog")[0];
    return dialogComponent.internals_.shadowRoot;
  }

  static show = ({
    title = "Error",
    description = "",
    callback = () => {},
  }) => {
    const listener = () => {
      Dialog.close();
      callback();
    };
    Dialog.getDialog()
      .getElementById("okButton")
      .removeEventListener("click", listener);
    Dialog.getDialog().getElementById("dialog").classList.remove("hide");
    Dialog.getDialog().getElementById("title").textContent = title;
    Dialog.getDialog().getElementById("description").textContent = description;
    Dialog.getDialog()
      .getElementById("okButton")
      .addEventListener("click", listener);
  };

  static close = () => {
    Dialog.getDialog().getElementById("dialog").classList.add("hide");
  };
}

customElements.define("general-dialog", Dialog);
