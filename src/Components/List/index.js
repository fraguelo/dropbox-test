import { getFiles } from "../../libs/dropbox";
import { sortByName } from "../../libs/utils";
import Item from "../Item";
import Dialog from "../Dialog";
import styles from "./styles.css";

let downloading = false;
let entries;
let sortBy = "sortDate";
const breadcrumb = [{ label: "Home", path: "" }];
export default class List extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
    const style = document.createElement("style");
    style.textContent = styles;
    this.shadowRoot.appendChild(style);
  }

  toggleDownload() {
    downloading = !downloading;
  }

  isDownloading() {
    return downloading;
  }

  getOrderButtons(fn) {
    Array.from(
      this.shadowRoot
        .getElementById("sortButtons")
        .getElementsByClassName("sort")
    ).forEach(fn);
  }

  sort({ target }) {
    this.getOrderButtons((item) => item.classList.remove("selected"));
    target.classList.add("selected");
    this.shadowRoot.getElementById("files").innerHTML = "";
    sortBy = target.id;
    this.renderItems();
  }

  renderItems(items) {
    const filesContainer = this.shadowRoot.getElementById("files");
    filesContainer.innerHTML =
      entries && entries.length
        ? (sortBy === "sortDate" ? entries : sortByName(entries))
            ?.map((item) => {
              return `<general-item  
              name="${item.name}"
              date="${item.server_modified}"
              path="${item.path_lower}" 
              type="${item[".tag"]}" ></general-item>`;
            })
            .join("")
        : "";
  }

  async loadFiles(path = "") {
    try {
      const { result } = await getFiles(path);
      entries = result.entries;
    } catch (e) {
      Dialog.show({
        title: "Error",
        description: "Dropbox token expired",
        callback: () => {
          window.location.href = "/";
        },
      });
    }
  }

  renderBreadcrumb() {
    this.shadowRoot.getElementById("breadcrumb").innerHTML = breadcrumb
      .map(
        ({ label, path }, idx) =>
          `<a href='javascript:' onclick="this.getRootNode().host.goFromBreadcrumb('${path}',${idx})">${label}</a>`
      )
      .join(" / ");
  }

  goFromBreadcrumb(path, idx) {
    breadcrumb.splice(idx + 1);
    this.openFolder(path);
  }

  goFromItem(label, path) {
    breadcrumb.push({ label, path });
    this.openFolder(path);
  }

  async openFolder(path) {
    await this.loadFiles(path);
    this.renderItems();
    this.renderBreadcrumb();
  }

  async connectedCallback() {
    const div = document.createElement("div");
    div.innerHTML = `
    <div class="title">
      <div class="dropbox"><img src="src/img/dropbox.png" height="50" /></div>
      <div class="text">Dropbox</div>
      <div id="sortButtons">
        <span>Sort By:</span>
        <div id="sortDate" title='Sort By Date' class="sort selected" ></div>
        <div id="sortName" title='Sort By Name' class="sort" ></div>
      </div>
    </div>
    <div>
    <div id="breadcrumb"></div>
    <div id="files"></div> `;
    this.shadowRoot.append(div);
    this.getOrderButtons((item) =>
      item.addEventListener("click", this.sort.bind(this))
    );
    this.openFolder();
  }
}

customElements.define("general-list", List);
