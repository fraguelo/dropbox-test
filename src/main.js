import { isAuthenticated } from "./libs/dropbox";
import "./css/style.css";

import Login from "./Components/Login";
import List from "./Components/List";
import Dialog from "./Components/Dialog";
import { Router } from "@vaadin/router";

document.querySelector("#app").innerHTML = `<div id='outlet'>
  <general-dialog></general-dialog>
</div>`;
const outlet = document.getElementById("outlet");
const router = new Router(outlet);

router.setRoutes([
  {
    path: "/",
    component: isAuthenticated() ? "general-list" : "general-login",
  },
]);
